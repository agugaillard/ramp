package main

import (
	"gitlab.com/agugaillard/ramp/filesystem"
	"go.uber.org/zap"
)

var (
	logger, _ = zap.NewDevelopment()
)

func main() {
	root := filesystem.NewRootFile()
	a, _ := filesystem.NewFile("a", root)
	b, _ := filesystem.NewFile("b", a)
	aliasA := filesystem.NewAliasFile("alias_a", root, a)
	c, _ := filesystem.NewFile("c", aliasA)
	d, _ := filesystem.NewFile("d", root)
	e, _ := filesystem.NewFile("e", d)

	filesystem.DebugFilesystem(logger, "filesystem at /root", root)
	filesystem.DebugFilesystem(logger, "filesystem at /root/a", a)
	filesystem.DebugFilesystem(logger, "filesystem at /root/a/b", b)
	filesystem.DebugFilesystem(logger, "filesystem at /root/a/c", c)
	filesystem.DebugFilesystem(logger, "filesystem at /root/alias_a", aliasA)
	filesystem.DebugFilesystem(logger, "filesystem at /root/d", d)
	filesystem.DebugFilesystem(logger, "filesystem at /root/d/e", e)

	commonParent, _ := filesystem.CommonParent(a, b)
	logger.Debug("common parent",
		zap.String("file1", a.Name),
		zap.String("file2", b.Name),
		zap.String("parent pwd", commonParent.Pwd().String()))

	commonParent, _ = filesystem.CommonParent(b, c)
	logger.Debug("common parent",
		zap.String("file1", b.Name),
		zap.String("file2", c.Name),
		zap.String("parent pwd", commonParent.Pwd().String()))

	commonParent, _ = filesystem.CommonParent(aliasA, a)
	logger.Debug("common parent",
		zap.String("file1", aliasA.Name),
		zap.String("file2", a.Name),
		zap.String("parent pwd", commonParent.Pwd().String()))

	commonParent, _ = filesystem.CommonParent(e, c)
	logger.Debug("common parent",
		zap.String("file1", e.Name),
		zap.String("file2", c.Name),
		zap.String("parent pwd", commonParent.Pwd().String()))
}
