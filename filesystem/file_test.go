package filesystem_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/agugaillard/ramp/filesystem"
)

func Test_NewFile_whenFileAlreadyExists_shouldReturnErrFileAlreadyExists(t *testing.T) {
	root := filesystem.NewRootFile()

	_, err := filesystem.NewFile("a", root)
	assert.Nil(t, err)

	_, err = filesystem.NewFile("a", root)
	assert.Equal(t, filesystem.ErrFileAlreadyExists, err)
}

func Test_Pwd_whenFileIsRoot(t *testing.T) {
	root := filesystem.NewRootFile()
	pwd := root.Pwd()
	assert.Equal(t, 1, len(pwd))
	assert.Equal(t, "/root", pwd[0].Pwd().String())
}

func Test_Pwd_whenFileIsNotRoot(t *testing.T) {
	root := filesystem.NewRootFile()
	a, _ := filesystem.NewFile("a", root)
	b, _ := filesystem.NewFile("b", a)
	pwd := b.Pwd()
	assert.Equal(t, 3, len(pwd))
	assert.Equal(t, "/root/a/b", pwd.String())
}

func Test_Pwd_whenUsingAlias_shouldRemoveAlias(t *testing.T) {
	// root
	// - a
	// - - b
	// - alias_a
	// - - c
	root := filesystem.NewRootFile()
	a, _ := filesystem.NewFile("a", root)
	_, _ = filesystem.NewFile("b", a)
	aliasA := filesystem.NewAliasFile("alias_a", root, a)
	c, _ := filesystem.NewFile("c", aliasA)

	pwd := c.Pwd()
	assert.Equal(t, "/root/a/c", pwd.String())

	pwd = aliasA.Pwd()
	assert.Equal(t, "/root/a", pwd.String())
}
