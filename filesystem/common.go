package filesystem

import (
	"errors"
)

var (
	ErrUncommonRoot = errors.New("uncommon root")
)

func CommonParent(f1, f2 *file) (*file, error) {
	pwd1 := f1.Pwd()
	pwd2 := f2.Pwd()
	if pwd1[0] != pwd2[0] {
		return nil, ErrUncommonRoot
	}
	i := 1
	for {
		if i >= len(pwd1) || i >= len(pwd2) {
			break
		}
		if pwd1[i].id != pwd2[i].id {
			break
		}
		i++
	}
	return pwd1[i-1], nil
}
