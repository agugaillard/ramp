package filesystem_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/agugaillard/ramp/filesystem"
)

func Test_CommonParent(t *testing.T) {
	root := filesystem.NewRootFile()
	a, _ := filesystem.NewFile("a", root)
	b, _ := filesystem.NewFile("b", a)
	c, _ := filesystem.NewFile("c", a)

	commonParent, _ := filesystem.CommonParent(b, c)
	assert.Equal(t, "/root/a", commonParent.Pwd().String())

	commonParent, _ = filesystem.CommonParent(c, root)
	assert.Equal(t, "/root", commonParent.Pwd().String())
}

func Test_CommonParent_whenResultIsRoot(t *testing.T) {
	root := filesystem.NewRootFile()
	a, _ := filesystem.NewFile("a", root)
	b, _ := filesystem.NewFile("b", root)
	commonParent, _ := filesystem.CommonParent(a, b)
	assert.Equal(t, "/root", commonParent.Pwd().String())
}

func Test_CommonParent_whenUncommonRoot(t *testing.T) {
	root1 := filesystem.NewRootFile()
	root2 := filesystem.NewRootFile()
	a, _ := filesystem.NewFile("a", root1)
	b, _ := filesystem.NewFile("b", root2)
	commonParent, err := filesystem.CommonParent(a, b)
	assert.Nil(t, commonParent)
	assert.Equal(t, filesystem.ErrUncommonRoot, err)
}

func Test_CommonParent_whenUsingAlias(t *testing.T) {
	// root
	// - a
	// - - b
	// - alias_a
	// - - c
	root := filesystem.NewRootFile()
	a, _ := filesystem.NewFile("a", root)
	b, _ := filesystem.NewFile("b", a)
	aliasA := filesystem.NewAliasFile("alias_a", root, a)
	c, _ := filesystem.NewFile("c", aliasA)

	commonParent, _ := filesystem.CommonParent(b, c)
	assert.Equal(t, "/root/a", commonParent.Pwd().String())

	commonParent, _ = filesystem.CommonParent(a, c)
	assert.Equal(t, "/root/a", commonParent.Pwd().String())

	commonParent, _ = filesystem.CommonParent(aliasA, c)
	assert.Equal(t, "/root/a", commonParent.Pwd().String())
}
