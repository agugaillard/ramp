package filesystem

import (
	"errors"
	"strings"

	"github.com/google/uuid"
	"go.uber.org/zap"
)

const (
	ROOT_NAME = "root"
)

var (
	ErrFileAlreadyExists = errors.New("file already exists")
)

type file struct {
	id     string          `json:"-"`
	parent *file           `json:"-"`
	files  map[string]file `json:"files"`
	alias  *file           `json:"alias"`
	Name   string          `json:"name"`
}

type path []*file

func NewRootFile() *file {
	return &file{id: uuid.NewString(), Name: ROOT_NAME}
}

func NewFile(name string, parent *file) (*file, error) {
	file := &file{id: uuid.NewString(), Name: name}
	if err := parent.addFile(file); err != nil {
		return nil, err
	}
	file.parent = parent
	return file, nil
}

func NewAliasFile(name string, parent, target *file) *file {
	return &file{
		id:     uuid.NewString(),
		Name:   name,
		parent: parent,
		files:  target.files,
		alias:  target,
	}
}

func (f *file) addFile(child *file) error {
	if f.files != nil {
		if _, found := f.files[child.Name]; found {
			return ErrFileAlreadyExists
		}
	}
	if f.files == nil {
		f.files = make(map[string]file, 1)
	}
	f.files[child.Name] = *child
	if f.alias != nil {
		child.parent = f.alias
	} else {
		child.parent = f
	}
	return nil
}

func (f *file) Pwd() path {
	if f.parent != nil {
		currentFileIgnoringAlias := f
		if f.alias != nil {
			currentFileIgnoringAlias = f.alias
		}
		if f.parent.alias != nil {
			return append(f.parent.alias.Pwd(), currentFileIgnoringAlias)
		}
		return append(f.parent.Pwd(), currentFileIgnoringAlias)
	}
	return []*file{f}
}

func (p path) String() string {
	stringPaths := make([]string, len(p)+1)
	stringPaths[0] = ""
	for i, f := range p {
		stringPaths[i+1] = f.Name
	}
	return strings.Join(stringPaths, "/")
}

func DebugFilesystem(logger *zap.Logger, message string, file *file) {
	logger.Debug(message,
		zap.Any("name", file.Name),
		zap.Any("files", file.files),
		zap.Any("alias", file.alias),
		zap.Any("pwd", file.Pwd().String()))
}
