# Ramp

## Explanation
In order to calculate the lowest common ancestor
1. We get the two paths
   1. *O(h) where h is the height of the filesystem*
2. Compare (from left to right) until path doesn't match anymore
   1. *O(h)*
3. If file is an alias, when getting the path should always referentiate the source
    ```go
    // root
    // - a
    // - alias_a
    // - - b

    root := filesystem.NewRootFile()
	a, _ := filesystem.NewFile("a", root)
    aliasA := filesystem.NewAliasFile("alias_a", root, a)
    b, _ := filesystem.NewFile("b", aliasA)
    pwd = b.Pwd() // /root/a
    ```

## Run unit tests
```
docker compose up test
```

## Run example
```
docker compose up sample
```
